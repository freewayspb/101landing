<?php
$postType = 'news';
$keyOrder = null;
$postStatuses = array('publish');
$fields = array(
    'wpcf-news-title',
    'wpcf-news-image',
    'wpcf-news-read-more-text',
);

$news = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

?>
<?php if ( is_array($news) && !empty($news) ) { ?>
    <div class="news">
        <h2 class="news__title"><?php pll_e('News & Press'); ?></h2>
        <div class="news__slider js-news-slider">
            <?php foreach ( $news as $new) { ?>

                <div class="news__slider-item">
                    <div class="news__content">
                        <img src="<?php echo current($new['metadata']['wpcf-news-image']); ?>" alt="" class="news__content-item-image">
                        <h2 class="news__content-item-title">
                            <?php echo current($new['metadata']['wpcf-news-title']); ?>
                        </h2>
                        <p class="news__content-item-description">
                            <?php echo do_shortcode($new['DATA']->post_excerpt); ?>
                        </p>
                        <div class="news__content-item-social">
                            <div class="news__content-item-social-time">
                                <?php echo date('d.m.Y', strtotime($new['DATA']->post_date)); ?>
                            </div>
                            <a href="<?php echo get_permalink($new['DATA']->ID); ?>"
                               class="news__content-item-social-link"
                            ><?php echo current($new['metadata']['wpcf-news-read-more-text']); ?></a>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>
<?php }
