<?php
$postType = 'for-whom';
$keyOrder = 'wpcf-for-whom-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-for-whom-text',
    'wpcf-for-whom-image',
    'wpcf-for-whom-css-style-postfix',
);

$forWhoms = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
if (is_array($forWhoms) && !empty($forWhoms)) { ?>
    <div class="members">
        <div class="members__inner">
            <h3 class="members__title"><?php pll_e('for whom'); ?></h3>

            <?php foreach($forWhoms as $forWhom ) {  $mData = $forWhom['metadata'];
                $image = current($mData['wpcf-for-whom-image']);
                $text = current($mData['wpcf-for-whom-text']);  ?>
                <div class="members__item">
                    <div class="members__item-icon members__item-icon_<?php echo current($mData['wpcf-for-whom-css-style-postfix']); ?>"
                        <?php if ($image) { ?> style="background-image: url('<?php echo $image; ?>')" <?php } ?>
                    ></div>
                    <h4 class="members__item-title"><?php echo $forWhom['title']; ?></h4>
                </div>
            <?php } ?>

        </div>
    </div>
<?php }