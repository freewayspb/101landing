<?php

$postType = 'features-slider';
$keyOrder = 'wpcf-features-slider-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-features-slider-text',
    'wpcf-features-slider-image',
    'wpcf-features-slider-button-text',
    'wpcf-features-slider-button-url',
);

$featuresSliders = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);


if(is_array($featuresSliders) && !empty($featuresSliders)) {
    $currentFeaturesSlider = current($featuresSliders);
    $text = '';
    $buttonText = '';
    $buttonUrl = '#';
    if (isset($currentFeaturesSlider['metadata'])) {
        $text = current($currentFeaturesSlider['metadata']['wpcf-features-slider-text']);
        $buttonText = current($currentFeaturesSlider['metadata']['wpcf-features-slider-button-text']);
        $buttonUrl = current($currentFeaturesSlider['metadata']['wpcf-features-slider-button-url']);
    }
    ?>
    <div class="top-slider">
        <h1 class="top-slider__title"><?php echo $text; ?></h1>
        <div class="top-slider__slider js-top-slider">
            <?php
            foreach ($featuresSliders as $featuresSlider) { $mData = $featuresSlider['metadata']; ?>
                <div class="top-slider__slider-item top-slider__slider-item_one" style="background-image: url(<?php echo current($mData['wpcf-features-slider-image']); ?>)"></div>
            <?php  } ?>
        </div>
        <a href="<?php echo $buttonUrl; ?>" class="top-slider__button"><?php echo $buttonText; ?></a>
    </div>
<?php } 
