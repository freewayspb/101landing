<?php
$postType = 'cloud-services';
$keyOrder = 'wpcf-cloud-services-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-cloud-services-image',
);

$cloudServices = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$postType = '_servers';
$keyOrder = 'wpcf-servers-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-servers-image',
);

$servers = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

?>

<?php if ( is_array($cloudServices) && !empty($cloudServices)
    && is_array($servers) && !empty($servers) ) { ?>

    <div class="packages" >
        <div class="packages__inner" >
            <h3 class="packages__title"><?php pll_e('ADD-ONs'); ?></h3>
            <div class="servers">
                <div class="servers__inner">
                    <div class="servers__tabs">
                        <a href="#" data-tab="cloud"  class="servers__tabs-link servers__tabs-link_active js-servers-tab-link"
                        ><?php pll_e('Cloud  services'); ?></a>
                        <a href="#" data-tab="servers" class="servers__tabs-link js-servers-tab-link"
                        ><?php pll_e('Servers'); ?></a>
                    </div>
                    <div class="servers__tabs-content js-servers-slider-container">
                        <div data-tab="cloud" class="servers__tabs-slider servers__tabs-slider_current js-cloud-slider">
                            <?php foreach($cloudServices as $cloudService ) { $mData = $cloudService['metadata']; ?>
                                <div class="servers__tabs-slider-item">
                                    <div class="servers__tabs-slider-item-icon">
                                        <img src="<?php echo current($mData['wpcf-cloud-services-image']); ?>"
                                             alt="" class="servers__tabs-slider-item-icon-image">
                                    </div>
                                    <p class="servers__tabs-slider-item-text"><?php echo $cloudService['title']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div data-tab="servers" class="servers__tabs-slider js-servers-slider">
                            <?php foreach ($servers as $server) { $mData = $server['metadata']; ?>
                                <div class="servers__tabs-slider-item">
                                    <div class="servers__tabs-slider-item-icon">
                                        <img src="<?php echo current($mData['wpcf-servers-image']); ?>"
                                             alt="" class="servers__tabs-slider-item-icon-image">
                                    </div>
                                    <p class="servers__tabs-slider-item-text"><?php echo $server['title']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>