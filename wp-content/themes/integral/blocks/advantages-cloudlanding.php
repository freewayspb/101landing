<?php
$postType = 'advantages';
$keyOrder = 'wpcf-advantages-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-advantages-text',
    'wpcf-advantages-image',
    'wpcf-advantages-onhover-text'
);

$advantages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
if (is_array($advantages) && !empty($advantages)) { ?>
    <div class="advantages">
        <div class="advantages__inner">
            <h3 class="advantages__title"><?php pll_e('advantages'); ?></h3>
            <?php foreach($advantages as $adv ) {  $mData = $adv['metadata']; ?>
                <div class="advantages__item">
                    <img class="advantages__item-icon" src="<?php echo current($mData['wpcf-advantages-image']); ?>" alt="">
                    <p class="advantages__item-description"><?php echo current($mData['wpcf-advantages-text']); ?></p>
                    <div class="advantages__item-hidden">
                        <div class="advantages__item-hidden-content">
                            <?php echo current($mData['wpcf-advantages-onhover-text']); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php }