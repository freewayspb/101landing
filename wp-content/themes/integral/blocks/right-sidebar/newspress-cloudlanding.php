<?php
$postType = 'news';
$keyOrder = null;
$postStatuses = array('publish');
$fields = array(
    'wpcf-news-title',
    'wpcf-news-image',
    'wpcf-news-read-more-text',
);

$numberPosts = 3;
$news = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null, false, array(), $numberPosts);



?>
<?php if ( is_array($news) && !empty($news) ) { ?>
    <div class="news-page__aside">
        <h2 class="news-page__aside-title"><?php pll_e('More News'); ?></h2>

            <?php foreach ( $news as $new) { $mData = $new['metadata']; ?>
                <div class="news-page__aside-item">
                    <img class="news-page__aside-item-image" src="<?php echo current($mData['wpcf-news-image']); ?>" alt="">
                    <h3 class="news-page__aside-item-title"><?php echo current($mData['wpcf-news-title']); ?></h3>
                    <div class="news-page__aside-item-text"><?php echo do_shortcode($new['DATA']->post_excerpt); ?></div>
                    <div class="news-page__aside-item-info">
                        <span class="news-page__aside-item-info-time"><?php echo date('d.m.Y', strtotime($new['DATA']->post_date)); ?></span>
                        <a class="news-page__aside-item-info-link" href="<?php echo get_permalink($new['DATA']->ID); ?>"
                        ><?php echo current($mData['wpcf-news-read-more-text']); ?></a>
                    </div>
                </div>
            <?php } ?>
    </div>
<?php } ?>
