<?php
$postType = 'packages';
$keyOrder = 'wpcf-packages-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-packages-number-of-users',
    'wpcf-packages-services-1',
    'wpcf-packages-cost-per-month',
    'wpcf-packages-cost-from',
    'wpcf-packages-feature',
    'wpcf-packages-buy-url',
);
$multipleOn = 2;
$cntPackages = 0;
$_pckges = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
if (is_array($_pckges)) {
    $cntPackages = count($_pckges);
}
$packages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, $multipleOn, null );
?>
<?php if (is_array($packages) && !empty($packages)) { ?>
    <div class="packages">
        <input type="hidden"  id="js-packages-slides-number" name="js-packages-slides-number" value="<?php echo $cntPackages; ?>" />
        <div class="packages__inner">
            <h3 class="packages__title"><?php pll_e('packages'); ?></h3>
            <div class="packages__slider js-packages-slider">
                <?php foreach ($packages as $package ) { $mData = $package['metadata'];
                    $features = explode("\n", current($package['metadata']['wpcf-packages-feature'])); ?>

                    <div class="packages__slider-block">
                        <div class="packages__slider-item">
                            <h4 class="packages__slider-item-title"><?php echo $package['title']; ?></h4>
                            <p class="packages__slider-item-price"
                            ><?php pll_e('From'); ?> <span class="packages__slider-item-dollar">CHF</span>
                                <span class="packages__slider-item-count"><?php echo current($mData['wpcf-packages-cost-from']);
                                    ?></span> <?php pll_e('per day'); ?></p>
                            <ul class="packages__slider-item-list">
                                <li class="packages__slider-item-list-item"
                                ><?php pll_e('Up to '); echo current($mData['wpcf-packages-number-of-users']); pll_e('users'); ?></li>
                                <?php if (is_array($features) && !empty($features)) { ?>
                                    <?php foreach ($features as $feature) { ?>
                                        <li class="packages__slider-item-list-item" ><?php echo $feature; ?></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>

                            <?php $imageArray = preparePackagesImages($mData);
                            if (count($imageArray)){ ?>
                                <h5 class="packages__slider-item-integration"><?php pll_e('integration'); ?></h5>
                                <div class="packages__slider-item-logos">
                                    <?php foreach ($imageArray as $img) { ?>
                                        <img src="<?php echo $img; ?>" class="packages__slider-item-logos_item">
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <a href="<?php echo current($mData['wpcf-packages-buy-url']); ?>" target="_blank" class="packages__slider-item-btn"><?php pll_e('Buy'); ?></a>
                        </div>
                    </div>
                    <!-- <?php //echo 'Cost per month ' . current($mData['wpcf-packages-cost-per-month']) . ' $ '; ?> -->
                <?php } ?>
            </div>
            <p class="packages__privacy"><?php pll_e('*Price is not including Software licenses and necessary hardware'); ?></p>
        </div>
    </div>
<?php }