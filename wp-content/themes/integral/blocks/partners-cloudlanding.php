<?php
$postType = 'partners';
$keyOrder = 'wpcf-partners-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-partners-image',
);

$partners = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
?>
<?php if(is_array($partners) && !empty($partners)) { ?>
    <div class="partners">
        <div class="partners__inner">
            <h3 class="partners__title"><?php pll_e('partners'); ?></h3>
            <div class="partners__slider  js-partners-slider">
                <?php foreach($partners as $partner) { ?>
                    <div class="partners__slider-item">
                        <img class="partners__slider-item-icon" src="<?php echo current($partner['metadata']['wpcf-partners-image']); ?>" alt="">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } 