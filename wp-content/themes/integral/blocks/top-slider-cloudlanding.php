<?php

$postType = 'home-page-slider';
$keyOrder = 'wpcf-home-page-slider-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-home-page-slider-text',
    'wpcf-home-page-slider-image',
    'wpcf-home-page-slider-button-text',
    'wpcf-home-page-slider-button-url',
);

$homePageSliders = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);


if(is_array($homePageSliders) && !empty($homePageSliders)) { ?>
<div class="banner">
    <div class="banner__slider js-banner-slider">
        <?php
        foreach ($homePageSliders as $homePageSlider) { $mData = $homePageSlider['metadata']; ?>
            <div class="banner__slider-item banner__slider-item_one" style="background-image: url(<?php echo current($mData['wpcf-home-page-slider-image']); ?>)">
                <div class="banner__content">
                    <h2 class="banner__title"><?php echo current($mData['wpcf-home-page-slider-text']); ?></h2>
                    <div class="banner__description">
                        <?php echo current($mData['wpcf-home-page-slider-text']); ?>
                    </div>
                    <a href="<?php echo current($mData['wpcf-home-page-slider-button-url']); ?>"
                       class="banner__button"><?php echo current($mData['wpcf-home-page-slider-button-text']); ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
    <?php }