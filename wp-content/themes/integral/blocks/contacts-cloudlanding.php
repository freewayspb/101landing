<?php

$postType = 'contacts';
$keyOrder = 'wpcf-contacts-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-contacts-title',
    'wpcf-contacts-image',
    'wpcf-contacts-details',
    'wpcf-contacts-url',
);

$contacts = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$postType = 'subscribe-head-block';
$keyOrder = 'wpcf-subscribe-head-block-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-subscribe-head-block-title',
    'wpcf-subscribe-head-block-description',
);
$shbs = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);


$postType = 'subscribe-foot-block';
$keyOrder = 'wpcf-subscribe-footer-block-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-subscribe-footer-block-description',
);

$sfbs = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

?>

<div class="contacts">
    <div class="contacts__inner">
        <h3 class="contacts__title"><?php pll_e('contact'); ?></h3>
        <?php if (is_array($contacts) && !empty($contacts)) { ?>
            <?php foreach($contacts as $contact ) {
                $mData = $contact['metadata'];
                $contactDetailLines = explode("\n", current($mData['wpcf-contacts-details']));
                $url = current($mData['wpcf-contacts-url']);
                if (!$url) { $url = '#'; }
                ?>
                <div class="contacts__item">
                    <a href="<?php echo $url;?>">
                        <img src="<?php echo current($mData['wpcf-contacts-image']);?>" alt="location" class="contacts__item-img">
                    </a>
                    <a href="<?php echo $url;?>">
                        <h4 class="contacts__item-title"><?php echo current($mData['wpcf-contacts-title']);?></h4>
                    </a>
                    <?php if (is_array($contactDetailLines) && !empty($contactDetailLines)) { ?>
                        <?php foreach ($contactDetailLines as $contactDetailLine) { ?>
                            <p class="contacts__item-information"><?php echo $contactDetailLine; ?></p>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>

        <?php if (
            (is_array($shbs) && !empty($shbs))
            ||
            (is_array($sfbs) && !empty($sfbs))
        ) { ?>

        <?php } ?>


        <?php get_template_part('block-socialmedia-cloudlanding'); ?>
    </div>
</div>
