<?php
$postType = 'what-we-do';
$keyOrder = 'wpcf-what-we-do-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-what-we-do-title',
    'wpcf-what-we-do-text',
);

$wwds = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
if (is_array($wwds) && !empty($wwds)) { ?>
    <div class="activity">
        <div class="activity__inner"><h3 class="activity__title"><?php pll_e('what we do'); ?></h3>
            <ul class="activity__list js-custom-scroll custom-scroll">
                <?php foreach($wwds as $wwd ) {  $mData = $wwd['metadata']; ?>
                    <li class="activity__list-item">
                        <h2 class="activity__list-item-title"><?php echo current($mData['wpcf-what-we-do-title']); ?></h2>
                        <?php echo current($mData['wpcf-what-we-do-text']); ?>
                    </li>
                <?php } ?>
            </ul>
            <img src="/wp-content/themes/integral/images/activity/macbooks.png" class="activity__macbook"></div>
    </div>
<?php } ?>