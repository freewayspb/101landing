<?php

$postType = 'social-media';
$keyOrder = 'wpcf-social-media-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-social-media-url',
    'wpcf-social-media-image',
    'wpcf-social-media-css-key-class',
);

$socialMedias = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null); ?>
<?php if(is_array($socialMedias)) { ?>
<div class="contacts__social-accounts">
<?php foreach($socialMedias as $socialMedia) {
    $image = current($socialMedia['metadata']['wpcf-social-media-image']);
    ?>
    <a target="_blank" href="<?php echo current($socialMedia['metadata']['wpcf-social-media-url']); ?>"
       class="contacts__social-accounts-item contacts__social-accounts-item_<?php
            echo current($socialMedia['metadata']['wpcf-social-media-css-key-class']); ?>"
       <?php if ($image) { ?> style="background: url('<?php echo $image; ?>') no-repeat" <?php } ?>
    ></a>
    <?php } ?>
</div>
<?php } ?>


