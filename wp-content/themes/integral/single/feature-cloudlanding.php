<?php

    $featuresUrl = '/';
    $featuresPage = getFeaturesPage();
    if ($featuresPage) {
        $featuresUrl = get_permalink($featuresPage);
    }

    $id = $post->ID;

    $postMeta = get_post_meta($id);

    $singlePageImage = '';
    if (isset($postMeta['wpcf-features-image-for-single-page'])) {
        $singlePageImage = current($postMeta['wpcf-features-image-for-single-page']);
    }
    $image = '';
    if (isset($postMeta['wpcf-features-image'])) {
        $image = current($postMeta['wpcf-features-image']);
    }


    $_ = getChildPostsIdsByParentId( $id, '_wpcf_belongs_feature_id' );

    $postType = 'feature-description';
    $keyOrder = 'wpcf-feature-description-order';
    $postStatuses = array('publish', 'private');
    $fields = array(
        'wpcf-feature-description-title',
        'wpcf-feature-description-text',
        'wpcf-feature-description-button-text',
        'wpcf-feature-description-button-url',
        'wpcf-feature-description-image',
        'wpcf-feature-description-image-on-hover',
        'wpcf-feature-description-order',
    );
    $children = array();
    if (is_array($_) && !empty($_)) {
        $children = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null, false, $_);
    }
?>

<div class="contract">
    <div class="contract__inner">
        <img class="contract__icon" alt="icon" src="<?php echo $image; ?>" alt="icon">
        <h3 class="contract__title"><?php echo $post->post_title; ?></h3>
        <p class="contract__text"><?php echo do_shortcode($post->post_content); ?></p>
    </div>
</div>

<div class="description">
    <div class="description__inner">
        <img src="<?php echo $singlePageImage; ?>" class="description__inner__img">
        <h3 class="description__title"><?php pll_e('description'); ?></h3>
        <div class="description__block">

        <?php foreach ($children as $_chld) { $mData = $_chld['metadata']; ?>
            <div class="description__block-item">
                <div class="description__block-item-wrapper">
                    <img src="<?php echo current($mData['wpcf-feature-description-image']); ?>"
                         alt="icon" class="description__block-item-img description__block-item-img_default">
                    <img src="<?php echo current($mData['wpcf-feature-description-image-on-hover']); ?>"
                         alt="icon" class="description__block-item-img description__block-item-img_hover"><!-- добавить картинку на hover-->
                </div>
                <p class="description__block-item-title"><?php echo current($mData['wpcf-feature-description-title']); ?></p>

                <p class="description__block-item-content">
                    <?php echo do_shortcode($_chld['DATA']->post_content); ?>
                </p>

                <a
                    class="description__block-item-btn"
                    href="<?php echo current($mData['wpcf-feature-description-button-url']); ?>"
                    ><?php echo current($mData['wpcf-feature-description-button-text']); ?></a>
            </div>
        <?php } ?>

            <a class="description__block-btn" href="<?php echo $featuresUrl; ?>"><?php pll_e('Back to Features'); ?></a>
        </div>

    </div>
</div>







