<?php

$mData = get_post_meta($post->ID);

$title = '';
if (isset($mData['wpcf-news-title'])) {
    $title = current($mData['wpcf-news-title']);
}
$image = '';
if (isset($mData['wpcf-news-image'])) {
    $image = current($mData['wpcf-news-image']);
}

$readmoreText = 'Read more';
if (isset($mData['wpcf-news-read-more-text'])) {
    $readmoreText = current($mData['wpcf-news-read-more-text']);
}

?>
<div class="news-page">
    <div class="news-page__inner">
        <div class="news-page__main-content">
            <h2 class="news-page__main-content-title"><?php echo $title; ?></h2>
            <span class="news-page__main-content-time"><?php echo date('d.m.Y', strtotime($post->post_date)); ?></span>
            <img class="news-page__main-content-image" src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
            <div class="news-page__main-content-text"
                ><?php echo do_shortcode($post->post_content); ?></div>

            <a href="/" class="news-page__main-content-button"><?php pll_e('Back to home page'); ?></a>
        </div>
        <?php get_template_part('blocks/right-sidebar/newspress-cloudlanding'); ?>
    </div>
</div>
