<?php
$postType = 'packages';
$keyOrder = 'wpcf-packages-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-packages-title',
    'wpcf-packages-number-of-users',
    'wpcf-packages-services-1',
    'wpcf-packages-cost-per-month',
    'wpcf-packages-cost-from',
    'wpcf-packages-cost-per-day',
    'wpcf-packages-feature',
    'wpcf-packages-buy-url',
);
$multipleOn = null;
$packages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, $multipleOn, null );





$postType = 'for-whom';
$keyOrder = 'wpcf-for-whom-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-for-whom-title-for-how-much-page',
    'wpcf-for-whom-text',
    'wpcf-for-whom-image',
    'wpcf-for-whom-image-on-hover',
    'wpcf-for-whom-css-style-postfix',
);

$forWhoms = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

?>

<?php if ( is_array($forWhoms) && !empty($forWhoms)) { ?>
    <div class="for-whom">
        <div class="for-whom__heading">
            <h1 class="for-whom__title"><?php strtoupper(pll_e('how much')); ?></h1>
        </div>
        <div class="for-whom__tab-bar tab-bar">
            <div class="tab-bar__heading">
                <ul class="tab-bar__nav">
                    <?php $isFirst = true;
                    foreach ( $forWhoms as $forWhom ) { $mData = $forWhom['metadata'];
                        $image = current($mData['wpcf-for-whom-image']);
                        $imageOnHover = current($mData['wpcf-for-whom-image-on-hover']);
                        $title = $forWhom['title'];
                        if (!$title) {
                            $title = '&nbsp';
                        }

                        ?>
                        <li class="tab-bar__nav-item <?php if ($isFirst) { ?> tab-bar__nav-item_active <?php } ?> js-tab">
                            <div class="tab-bar__nav-inner">
                                <span class="tab-bar__nav-icon">
                            <span class="tab-bar__nav-icon-default"
                                <?php if ($image) { ?> style="background-image: url('<?php echo $image; ?>')" <?php } ?>
                            ></span>
                            <span class="tab-bar__nav-icon-hover"
                                <?php if ($imageOnHover) { ?>
                                    style="background-image: url('<?php echo $imageOnHover; ?>');"
                                <?php } ?>
                            ></span>
                        </span>
                                <span class="tab-bar__nav-title"><?php echo $forWhom['title']; ?></span>
                                <span class="tab-bar__nav-text"><?php echo current($mData['wpcf-for-whom-title-for-how-much-page']); ?></span>
                            </div>
                        </li>
                        <?php $isFirst = false; } ?>
                </ul>
            </div>
            <div class="tab-bar__content">
                <?php
                    $isFirst = true;
                    foreach ($forWhoms as $forWhom) {
                        $mData = $forWhom['metadata'];
                        $text = $forWhom['DATA']->post_content;
                        $title = '';
                        if (isset($forWhom['DATA']->post_title)) {
                            $title = $forWhom['DATA']->post_title;
                        }
                        ?>
                        <div class="tab-bar__item <?php if ($isFirst) { ?> tab-bar__item_active <?php  } ?> js-tab-content">
                            <div class="partner">
                                <h2 class="partner__title"><?php echo $title;?></h2>
                                <div class="partner__inner">
                                    <div class="partner__container-list">
                                        <?php echo do_shortcode($text);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $isFirst = false; } ?>
            </div>
        </div>
    </div>
<?php } 