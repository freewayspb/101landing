<?php

$returnUrl = '/';
if (is_array($_SERVER)) {
    if (isset($_SERVER['REQUEST_SCHEME']) && isset($_SERVER['SERVER_NAME'])
        && isset($_SERVER['PHP_SELF']) && isset($_SERVER['REQUEST_URI']) ) {
        $returnUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . $_SERVER['REQUEST_URI'];
    }
}

$languagesList = array();
if (function_exists('pll_languages_list')) {
    $languagesList = pll_languages_list();
}


?>

<?php if (false) { ?>
<div style="margin: 50px 600px">
<!--    <div class="contacts__subscribe-form" id="hidden-subscribe-now-form" style="display: none">--><?php
//        $postSample = get_post(2);
//        echo do_shortcode($postSample->post_content); ?>
<!--    </div>-->
<!--<form id="salesforce-form" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">-->

<!--    <input type="hidden" name="oid" value="00D0Y000000qn3E">-->
<!--    <input type="hidden" name="retURL" value="--><?php //echo $returnUrl; ?><!--">-->
<!---->
<!--    <!--  ----------------------------------------------------------------------  -->-->
<!--    <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->-->
<!--    <!--  these lines if you wish to test in debug mode.                          -->-->
<!--    <!--  <input type="hidden" name="debug" value=1>                              -->-->
<!--    <!--  <input type="hidden" name="debugEmail"                                  -->-->
<!--    <!--  value="r.huebinger@101-cloud.com">                                      -->-->
<!--    <!--  ----------------------------------------------------------------------  -->-->
<!--    <label for="title">--><?php //pll_e('Title'); ?><!--*</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Title'); ?><!--" id="title" maxlength="250" name="title" size="40" type="text"><br>-->
<!--    <label for="first_name">--><?php //pll_e('First Name'); ?><!--*</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('First Name'); ?><!--" id="first_name" maxlength="40" name="first_name" size="20" type="text"><br>-->
<!--    <label for="last_name">--><?php //pll_e('Last Name'); ?><!--*</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Last Name'); ?><!--" id="last_name" maxlength="80" name="last_name" size="20" type="text"><br>-->
<!--    <label for="middle_name">--><?php //pll_e('Middle Name'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Middle Name'); ?><!--" id="middle_name" maxlength="80" name="middle_name" size="20" type="text"><br>-->
<!--    <label for="suffix">--><?php //pll_e('Suffix'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Suffix'); ?><!--" id="suffix" maxlength="80" name="suffix" size="20" type="text"><br>-->
<!--    <label for="contact_via">--><?php //pll_e('Contact via:'); ?><!--</label>-->
<!--    <input class="contacts__subscribe-form-input" id="contact_via_phone" name="contact_via" value="phone" type="radio">--><?php //pll_e('Phone Number'); ?><!--<br>-->
<!--    <input class="contacts__subscribe-form-input" id="contact_via_email" name="contact_via" value="email" type="radio">--><?php //pll_e('E-mail Address'); ?><!--<br>-->
<!--    <label for="email">--><?php //pll_e('E-mail'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('E-mail'); ?><!--" id="email" maxlength="80" name="email" size="20" type="text"><br>-->
<!--    <label for="country_code">--><?php //pll_e('Country code'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Country code'); ?><!--" id="country_code" maxlength="80" name="country_code" size="20" type="text"><br>-->
<!--    <label for="area_code">--><?php //pll_e('Area code'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Area code'); ?><!--" id="area_code" maxlength="80" name="area_code" size="20" type="text"><br>-->
<!--    <label for="phone">--><?php //pll_e('Phone Number'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Phone Number'); ?><!--" id="phone" maxlength="40" name="phone" size="20" type="text"><br>-->
<!--    <label for="company">--><?php //pll_e('Company Name'); ?><!--*</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Company'); ?><!--" id="company" maxlength="40" name="company" size="20" type="text"><br>-->
<!--    <label for="company">--><?php //pll_e('Preferred Language'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Preferred Language'); ?><!--" id="preferred_language" maxlength="40" name="company" size="20" type="text"><br>-->
<!--    <label for="company">--><?php //pll_e('Interested in'); ?><!--</label><input class="contacts__subscribe-form-input" placeholder="--><?php //pll_e('Interested in'); ?><!--" id="interested_in" maxlength="40" name="company" size="20" type="text"><br>-->
<!--    <label for="subscribe_us">--><?php //pll_e('Subscribe us'); ?><!--</label><input class="contacts__subscribe-form-checkbox"  id="subscribe_us" maxlength="40" name="subscribe_us" size="20" type="checkbox"><br>-->

    <!--
    <label for="city"><?php pll_e('City'); ?></label><input class="contacts__subscribe-form-input" placeholder="<?php pll_e('City'); ?>" id="city" maxlength="40" name="city" size="20" type="text"><br>
    <label for="state"><?php pll_e('State/Province'); ?></label><input class="contacts__subscribe-form-input" placeholder="<?php pll_e('State/Province'); ?>" id="state" maxlength="20" name="state" size="20" type="text"><br>
    <label for="street"><?php pll_e('Street'); ?></label><textarea class="contacts__subscribe-form-input" placeholder="<?php pll_e('Street'); ?>" name="street"></textarea><br>
    <label for="URL"><?php pll_e('Website'); ?></label><input class="contacts__subscribe-form-input" placeholder="<?php pll_e('Website'); ?>" id="URL" maxlength="80" name="URL" size="20" type="text"><br>
    <label for="salutation"><?php pll_e('Salutation'); ?></label>
    <select class="contacts__subscribe-form-input" placeholder="<?php pll_e('Salutation'); ?>" id="salutation" name="salutation">
        <option value=""><?php pll_e('--None--'); ?></option>
        <option value="Mr."><?php pll_e('Mr.'); ?></option>
        <option value="Ms."><?php pll_e('Ms.'); ?></option>
        <option value="Mrs."><?php pll_e('Mrs.'); ?></option>
        <option value="Dr."><?php pll_e('Dr.'); ?></option>
        <option value="Prof."><?php pll_e('Prof.'); ?></option>
    </select><br>
    -->

<!--    <input style="display: none;" type="submit" value="submit" id="salesforce-form-submit" />-->
<!--    <input class="contacts__subscribe-form-btn" id="salesforce-form-button" type="button" name="submit" value="--><?php //_e('Submit', 'integral'); ?><!--">-->
</form>
</div>
<?php } ?>





<div class="contact-us">
    <div class="contact-us__inner">
        <h3 class="contact-us__title"><?php pll_e('Contact Us'); ?></h3>
        <p class="contact-us__description"><?php pll_e('You\'ll hear from a representative within one business day.'); ?></p>

        <!-- This hidden form is needed to send to our server for subscribing the customer -->
        <div class="contacts__subscribe-form" id="hidden-subscribe-now-form" style="display: none"><?php
            $postSample = get_post(2);
            echo do_shortcode($postSample->post_content); ?>
        </div>
        <form class="contact-us__form" id="salesforce-form" action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" >
            <input type="hidden" name="oid" value="00D0Y000000qn3E">
            <input type="hidden" name="retURL" value="<?php echo $returnUrl; ?>">

            <!--  ----------------------------------------------------------------------  -->
            <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
            <!--  these lines if you wish to test in debug mode.                          -->
            <!--  <input type="hidden" name="debug" value=1>                              -->
            <!--  <input type="hidden" name="debugEmail"                                  -->
            <!--  value="r.huebinger@101-cloud.com">                                      -->
            <!--  ----------------------------------------------------------------------  -->

            <div class="contact-us__form-group">
                <div class="contact-us__form-group-wrap-input">
                    <label class="contact-us__form-label"><?php pll_e('Title'); ?>*</label>
                    <select class="input">
                        <option value="Select" selected disabled>Select</option>
                        <option value="Mr."><?php pll_e('Mr.'); ?></option>
                        <option value="Ms"><?php pll_e('Ms'); ?></option>
                        <?php if (false) { ?>
                            <!--                    <option value="">--><?php //pll_e('--None--'); ?><!--</option>-->
                            <!--                    <option value="Mrs.">--><?php //pll_e('Mrs.'); ?><!--</option>-->
                            <!--                    <option value="Dr.">--><?php //pll_e('Dr.'); ?><!--</option>-->
                            <!--                    <option value="Prof.">--><?php //pll_e('Prof.'); ?><!--</option>-->
                        <?php } ?>
                    </select>
                </div>
                <div class="contact-us__form-group-wrap-input">
                    <label class="contact-us__form-label"><?php pll_e('First Name'); ?>*</label>
                    <input type="text" id="first_name" placeholder="<?php pll_e('Enter First Name'); ?>" class="input">
                </div>
            </div>

            <div class="contact-us__form-group">
                <label class="contact-us__form-label"><?php pll_e('Last Name'); ?>*</label>
                <input type="text" id="last_name" placeholder="<?php pll_e('Enter Last Name'); ?>" class="input">
                <a href="#" class="contact-us__form-group-link js-add-suffix-field"><?php pll_e('Add Middle Name or Suffix'); ?></a>
            </div>

            <div class="contact-us__form-group js-hidden-middle-name">
                <label for="" class="contact-us__form-label">Middle Name</label>
                <input type="text" placeholder="Enter Middle Name" class="input">
            </div>

            <div class="contact-us__form-group js-hidden-suffix">
                <label for="" class="contact-us__form-label">Suffix</label>
                <input type="text" placeholder="Enter Suffix" class="input">
            </div>

<!--            <label for="middle_name">--><?php //pll_e('Middle Name'); ?><!--</label>-->
<!--            <input -->
<!--                class="contacts__subscribe-form-input" -->
<!--                placeholder="--><?php //pll_e('Middle Name'); ?><!--" -->
<!--                id="middle_name" maxlength="80" name="middle_name" size="20" type="text">-->
<!--            <label for="suffix">--><?php //pll_e('Suffix'); ?><!--</label>-->
<!--            <input class="contacts__subscribe-form-input" -->
<!--                   placeholder="--><?php //pll_e('Suffix'); ?><!--" -->
<!--                   id="suffix" maxlength="80" name="suffix" size="20" type="text">-->

            <div class="contact-us__form-group contact-us-via">
                <label for="contact_via" class="contact-us__form-label"><?php pll_e('Contact via:'); ?></label>
                <input type="radio" class="radio-button js-radio-button" id="contact_via_phone" name="contact_via" value="phone" checked /><span><?php pll_e('Phone Number'); ?></span>
                <input type="radio" class="radio-button radio-button-hidden js-radio-button" id="contact_via_email" name="contact_via" value="email" /><span><?php pll_e('E-mail Address'); ?></span>
            </div>

            <div class="contact-us__form-group contact-us__form-group-tab contact-us__form-group_hidden contact-us__form-group_email">
                <label for="email" class="contact-us__form-label"><?php pll_e('E-mail'); ?></label>
                <input type="text" id="email" placeholder="<?php pll_e('Enter Your E-mail'); ?>" class="input">
            </div>
            
            <div class="contact-us__form-group contact-us__form-group-tab contact-us__form-group_phone">
                <div class="contact-us__form-group-wrap">
                    <label class="contact-us__form-label"><?php pll_e('Country code'); ?></label>
                    <select class="input" id="country_code" >
                        <option selected disabled>Select</option>
                        <?php foreach (getCountryCodes() as $object ) { ?>
                            <option value="<?php echo $object['name']; ?>" ><?php echo $object['numeric']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="contact-us__form-group-wrap">
                    <label for="area_code" class="contact-us__form-label"><?php pll_e('Area code'); ?></label>
                    <input id="area_code" type="text" class="input" placeholder="<?php pll_e('Enter Code'); ?>">
                </div>

                <div class="contact-us__form-group-wrap">
                    <label for="phone" class="contact-us__form-label"><?php pll_e('Phone Number'); ?></label>
                    <input id="phone" type="text" class="input" placeholder="<?php pll_e('Your Phone Number'); ?>">
                </div>
            </div>

            <div class="contact-us__form-group">
                <label for="company" class="contact-us__form-label"><?php pll_e('Company Name'); ?>*</label>
                <input id="company" type="text"  placeholder="<?php pll_e('Enter Company Name'); ?>" class="input">
            </div>

            <div class="contact-us__form-group">
                <label for="preferred_language"  class="contact-us__form-label"><?php pll_e('Preferred Language'); ?></label>
                <select class="input">
                    <option selected disabled><?php pll_e('Select'); ?></option>
                    <?php foreach ($languagesList as $oneLang) {
                        $name = getLanguageNameByCode($oneLang); ?>
                        <option value="<?php echo $name; ?>"><?php pll_e($name); ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="contact-us__form-group">
                <label for="interested_in" class="contact-us__form-label"><?php pll_e('Interested In'); ?></label>
                <select id="interested_in" name="select2" class="input js-other-interest">
                    <option selected disabled>Interested In</option>
                    <option value="DEMO">DEMO</option>
                    <option value="Technical">Technical Information</option>
                    <option value="Commercial Information">Commercial Information</option>
                    <option value="other">Others</option>
                </select>
            </div>

            <div class="contact-us__form-group js-hidden-interest">
                <label class="contact-us__form-label">Other Interested In</label>
                <input type="text" placeholder="Enter Interested In" class="input">
            </div>

            <div class="contact-us__form-group">
                <label class="contact-us__form-label"><?php pll_e('Subscribe to our newsletter'); ?>:</label>
                <p class="contact-us__form-group-checkbox">
                    <input type="checkbox" class="checkbox"  id="subscribe_us" ><?php pll_e('Recieve news'); ?>
                </p>
            </div>

            <!-- This hidden is used to send the form using javascript -->
            <input style="display: none;" type="submit" value="submit" id="salesforce-form-submit" />

            <button class="contact-us__form-button" id="salesforce-form-button" type="submit"><?php pll_e('Send'); ?></button>
        </form>
    </div>
</div>



<script type="text/javascript" >
function checkInput() {
    if( $('.contact-us-via > input:radio:checked').val() === 'email') {
        $('.js-switch-field').show();
        $('.js-hidden-field').hide();
    } else {
        $('.js-switch-field').hide();
        $('.js-hidden-field').css('display', 'inline-block');
    }
}
    $(document).ready(function() {

        checkInput();
        $('.contact-us-via > input:radio').click(checkInput);

        $('#salesforce-form-button').click(function () {

            var subscribe_us = $('#subscribe_us').is(':checked');

            if (!subscribe_us) {
                $('#salesforce-form-submit').trigger( "click" );
               return;
            }
            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var name = first_name + ' ' + last_name;
            var email = $('#email').val();
            var action = '#wpcf7-f187-o1';

            var _wpcf7 = $('#hidden-subscribe-now-form input[name=_wpcf7]').val();
            var _wpcf7_version = $('#hidden-subscribe-now-form input[name=_wpcf7_version]').val();
            var _wpcf7_locale = $('#hidden-subscribe-now-form input[name=_wpcf7_locale]').val();
            var _wpcf7_unit_tag = $('#hidden-subscribe-now-form input[name=_wpcf7_unit_tag]').val();
            var _wpnonce = $('#hidden-subscribe-now-form input[name=_wpnonce]').val();

//            var text664 = $('#hidden-subscribe-now-form input[name=text-664]').val(name);
//            var email158 = $('#hidden-subscribe-now-form input[name=email-158]').val(email);

            $.ajax({
                type: "POST",
                url: action,
                data: {
                    '_wpcf7': _wpcf7,
                    '_wpcf7_version': _wpcf7_version,
                    '_wpcf7_locale': _wpcf7_locale,
                    '_wpcf7_unit_tag': _wpcf7_unit_tag,
                    '_wpnonce': _wpnonce,
                    'text-664': name,
                    'email-158': email,
                },
                success: function () { },
            }).done(function () {
                $('#salesforce-form-submit').trigger( "click" );
            });;

        });
    });
</script>
