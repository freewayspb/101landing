<?php

$postType = 'what-others-say';
$keyOrder = 'wpcf-what-others-say-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-what-others-say-comment',
    'wpcf-what-others-say-who-says',
    'wpcf-what-others-say-image',
);

$whatOthersSay = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null); ?>


<?php if(is_array($whatOthersSay) && !empty($whatOthersSay)) { ?>

    <div class="reviews">
        <div class="reviews__inner">
            <h3 class="reviews__title"><?php pll_e('what others say'); ?></h3>
            <div class="reviews__slider js-reviews-slider">
                <?php foreach($whatOthersSay as $wos ) { $mData = $wos['metadata']; ?>
                    <div class="reviews__slider-item">
                        <img src="<?php echo current($mData['wpcf-what-others-say-image']); ?>"
                             alt="" class="reviews__slider-item-icon"><h4
                            class="reviews__slider-item-title"><?php echo current($mData['wpcf-what-others-say-who-says']); ?></h4>
                        <p class="reviews__slider-item-position"><?php echo current($mData['wpcf-what-others-say-author-position']); ?></p>
                        <div class="reviews__slider-item-quote">
                            <?php echo '"' . current($mData['wpcf-what-others-say-comment']) . '"'; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } 