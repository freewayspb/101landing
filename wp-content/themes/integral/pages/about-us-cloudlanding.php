<?php 

$postType = 'our-teams';
$keyOrder = 'wpcf-partners-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-our-team-name',
    'wpcf-our-team-text',
    'wpcf-our-team-image'
);
$ourTeams = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$postType = 'about-us-first-block';
$keyOrder = 'wpcf-about-us-first-block-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-about-us-first-block-header-title-1',
    'wpcf-about-us-first-block-header-title-2',
    'wpcf-about-us-first-block-content-text',
);
$aboutUsFirstBlocks = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$postType = 'about-us-secondblock';
$keyOrder = 'wpcf-about-us-second-block-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-about-us-second-block-title',
    'wpcf-about-us-second-block-text',
);
$aboutUsSecondBlocks = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$postType = 'about-us-our-values';
$keyOrder = 'wpcf-about-us-our-values-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-about-us-our-values-text',
);
$aboutUsOurValues = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

?>

<?php if (is_array($aboutUsFirstBlocks) && !empty($aboutUsFirstBlocks)) { ?>
    <div class="company">
        <div class="company__inner">
        <?php foreach ($aboutUsFirstBlocks as $aboutUsFirstBlock ) { $mData = $aboutUsFirstBlock['metadata']; ?>
            <h3 class="company__title"><?php echo current($mData['wpcf-about-us-first-block-header-title-1']); ?></h3>
            <h4 class="company__technology"><?php echo current($mData['wpcf-about-us-first-block-header-title-2']); ?></h4>
            <?php
                $text = current($mData['wpcf-about-us-first-block-content-text']);
                $_text = explode("\n", $text);
                if (is_array($_text) && !empty($_text)) {
                    foreach ($_text as $_line) { ?>
                        <p class="company__technology-text">
                            <?php echo $_line; ?>
                        </p>
                    <?php } ?>
                <?php } ?>
            <?php break;  ?>
        <?php } ?>
        </div>
    </div>
<?php } ?>



<?php if (is_array($aboutUsSecondBlocks) && !empty($aboutUsSecondBlocks)) { ?>
<div class="values">
    <div class="values__inner">
    <?php foreach ($aboutUsSecondBlocks as $aboutUsSecondBlock ) { $mData = $aboutUsSecondBlock['metadata']; ?>

        <div class="values__wrapper"><h4 class="values__wrapper-title"><?php echo current($mData['wpcf-about-us-second-block-title']); ?></h4>
            <div class="values__wrapper-align">
                <?php if (is_array($aboutUsOurValues) && !empty($aboutUsOurValues)) {
                    foreach ($aboutUsOurValues as $aboutUsOurValue) { $_mData = $aboutUsOurValue['metadata']; ?>
                        <div class="values__wrapper-item">
                            <p class="values__wrapper-item-text">
                                <?php echo current($_mData['wpcf-about-us-our-values-text']); ?>
                            </p>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="values__wrapper-wrap-text">
                <h3 class="values__wrapper-text">
                    <?php $_text = explode("\n", current($mData['wpcf-about-us-second-block-text']));
                    if (is_array($_text) && !empty($_text)) {
                        foreach ($_text as $_line) { ?>
                            <?php echo $_line; ?>
                        <?php } ?>
                    <?php } ?>
                </h3>
            </div>
        </div>
        <?php break;  ?>
    <?php } ?>
    </div>
</div>
<?php } ?>

<?php  if(false && is_array($ourTeams)) {
    foreach($ourTeams as $member) {  ?>
        <h5><?php pll_e('Name'); ?>: <?php echo current($member['metadata']['wpcf-our-team-name']); ?></h5>
        <h5><?php pll_e('Text'); ?>: <?php echo current($member['metadata']['wpcf-our-team-text']); ?></h5>
        <img style="width: 100px" src="<?php echo current($member['metadata']['wpcf-our-team-image']); ?>" />
        <?php
    }
}
