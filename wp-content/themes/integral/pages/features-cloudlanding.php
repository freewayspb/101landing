
<?php get_template_part('blocks/feature-slider-cloudlanding'); ?>


<?php
$postType = 'feature';
$keyOrder = 'wpcf-features-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-features-image',
    'wpcf-features-description',
    'wpcf-features-text-on-hover',
    'wpcf-features-url',
);
$features = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$_childs = array();
if(is_array($features)) {
    foreach($features as $feature) {
        $id = $feature['id'];
        $_ = getChildPostsIdsByParentId( $id, '_wpcf_belongs_feature_id' );

        $postType = 'feature-description';
        $keyOrder = 'wpcf-feature-description-order';
        $postStatuses = array('publish', 'private');
        $fields = array(
            'wpcf-feature-description-title',
            'wpcf-feature-description-text',
            'wpcf-feature-description-button-text',
            'wpcf-feature-description-button-url',
            'wpcf-feature-description-image',
            'wpcf-feature-description-image-on-hover',
            'wpcf-feature-description-order',
        );
        $children = array();
        if (is_array($_) && !empty($_)) {
            $children = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null, false, $_);
        }
        $_childs[$id] = $children;
    }
}


if(is_array($features)) { ?>
    <div class="features">
        <div class="features__inner">
            <h3 class="features__title"><?php pll_e('features'); ?></h3>
            <div class="features__block">
                <?php foreach($features as $feature) { ?>
                    <div class="features__block-item">
                        <img
                            src="<?php echo current($feature['metadata']['wpcf-features-image']); ?>"
                            alt="icon"
                            class="features__block-item-img"
                        >
                        <p class="features__block-item-title"
                        ><?php echo current($feature['metadata']['wpcf-features-description']); ?></p>
                        <a href="<?php echo $feature['url']; ?>"
                           class="features__block-item-hidden"><div class="features__block-item-hidden-content">
                            <?php
                            echo current($feature['metadata']['wpcf-features-text-on-hover']);
                            ?></div></a>



                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php }  ?>