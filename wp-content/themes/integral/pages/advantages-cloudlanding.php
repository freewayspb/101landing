<?php
$postType = 'advantages';
$keyOrder = 'wpcf-advantages-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-advantages-text',
    'wpcf-advantages-image',
    'wpcf-advantages-onhover-text'
);

$advantages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);
if (is_array($advantages) && !empty($advantages)) { ?>
<!--    <div class="advantages">-->
<!--        <div class="advantages__inner">-->
<!--            <h3 class="advantages__title">--><?php //pll_e('advantages'); ?><!--</h3>-->
<!--            --><?php //foreach($advantages as $adv ) {  $mData = $adv['metadata']; ?>
<!--                <div class="advantages__item">-->
<!--                    <img src="--><?php //echo current($mData['wpcf-advantages-image']); ?><!--"-->
<!--                         alt="icon" class="advantages__item-icon">-->
<!--                    <p class="advantages__item-description">--><?php //echo current($mData['wpcf-advantages-text']); ?><!--</p>-->
<!--                    <div class="advantages__item-hidden">-->
<!--                        <div class="advantages__item-hidden-content">-->
<!--                            --><?php //echo current($mData['wpcf-advantages-onhover-text']); ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            --><?php //} ?>
<!--        </div>-->
<!--    </div>-->


    <div class="advantages">
        <div class="advantages__inner">
            <h3 class="advantages__title"><?php pll_e('advantages'); ?></h3>

            <div class="advantages__item">
                <img class="advantages__item-icon" src="/wp-content/themes/integral/images/advantages/scales.svg" alt="">
                <p class="advantages__item-description">Scales</p>
                <div class="advantages__item-hidden">
                    <div class="advantages__item-hidden-content">
                        Our solution grows with your business success and is scaled from very small to very large.
                    </div>
                </div>
            </div>

            <div class="advantages__item">
                <img class="advantages__item-icon" src="/wp-content/themes/integral/images/advantages/cost.svg" alt="">
                <p class="advantages__item-description">80% Reductionof cost</p>
                <div class="advantages__item-hidden">
                    <div class="advantages__item-hidden-content">
                        Our solution grows with your business success and is scaled from very small to very large.
                    </div>
                </div>
            </div>

            <div class="advantages__item">
                <img class="advantages__item-icon" src="/wp-content/themes/integral/images/advantages/time.svg" alt="">
                <p class="advantages__item-description">98% Reduction of time</p>

                <div class="advantages__item-hidden">
                    <div class="advantages__item-hidden-content">
                        Our solution grows with your business success and is scaled from very small to very large.
                    </div>
                </div>
            </div>

            <div class="advantages__item">
                <img class="advantages__item-icon" src="/wp-content/themes/integral/images/advantages/pay.svg" alt="">
                <p class="advantages__item-description">Pay per use</p>

                <div class="advantages__item-hidden">
                    <div class="advantages__item-hidden-content">
                        Our solution grows with your business success and is scaled from very small to very large.
                    </div>
                </div>
            </div>

            <div class="advantages__item">
                <img class="advantages__item-icon" src="/wp-content/themes/integral/images/advantages/security.svg" alt="">
                <p class="advantages__item-description">Security & compliance</p>

                <div class="advantages__item-hidden">
                    <div class="advantages__item-hidden-content">
                        Our solution grows with your business success and is scaled from very small to very large.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }