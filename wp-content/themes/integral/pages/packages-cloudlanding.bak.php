<?php
$postType = 'packages';
$keyOrder = 'wpcf-packages-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-packages-title',
    'wpcf-packages-number-of-users',
    'wpcf-packages-services-1',
    'wpcf-packages-cost-per-month',
    'wpcf-packages-cost-from',
    'wpcf-packages-cost-per-day',
    'wpcf-packages-feature',
    'wpcf-packages-buy-url',
);
$multipleOn = null;
$packages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, $multipleOn, null );
?>
<?php if (false) { ?>
    <?php if (is_array($packages) && !empty($packages)) { ?>
        <?php pll_e('packages'); ?>

        <?php foreach ($packages as $package ) { $mData = $package['metadata'];
            $features = explode("\n", current($package['metadata']['wpcf-packages-feature'])); ?>

            <?php echo $package['title']; ?>
            <?php echo do_shortcode($package['DATA']->post_content); ?>
            <?php pll_e('From'); ?>
            <?php echo current($mData['wpcf-packages-cost-from']); ?>
            <?php pll_e('per day'); ?>
            <?php pll_e('Up to '); echo current($mData['wpcf-packages-number-of-users']); pll_e('users'); ?>

            <?php if (is_array($features) && !empty($features)) { ?>
                <?php foreach ($features as $feature) { ?>
                    <?php echo $feature; ?>
                <?php } ?>
            <?php } ?>
            </ul>
            <?php $imageArray = preparePackagesImages($mData);
            if (count($imageArray)){ ?>
                <?php pll_e('integration'); ?>
                <?php foreach ($imageArray as $img) { ?>
                    <img src="<?php echo $img; ?>">
                <?php } ?>
            <?php } ?>
            <a href="<?php echo current($mData['wpcf-packages-buy-url']); ?>"
               target="_blank" ><?php pll_e('Buy'); ?></a>
            <!-- <?php //echo 'Cost per month ' . current($mData['wpcf-packages-cost-per-month']) . ' $ '; ?> -->
        <?php } ?>
    <?php } ?>
<?php } ?>

<?php if ( is_array($packages) && !empty($packages)) { ?>
    <div class="for-whom">
        <div class="for-whom__heading">
            <h1 class="for-whom__title"><?php strtoupper(pll_e('how much')); ?></h1>
        </div>
        <div class="for-whom__tab-bar tab-bar">
            <div class="tab-bar__heading">
                <ul class="tab-bar__nav">
                    <?php $isFirst = true;
                    foreach ( $packages as $package ) { $mData = $package['metadata'];
                        $title = current($mData['wpcf-packages-title']);
                        if (!$title) {
                            $title = '&nbsp';
                        }

                        ?>
                        <li class="tab-bar__nav-item <?php if ($isFirst) { ?> tab-bar__nav-item_active <?php } ?> js-tab">
                            <div class="tab-bar__nav-inner">
                                <span class="tab-bar__nav-icon tab-bar__nav-icon_<?php //echo current($mData['']); ?>">
                                    <span class="tab-bar__nav-icon-from"><?php pll_e('From'); ?></span>
                                    <span class="tab-bar__nav-icon-chf"><?php pll_e('CHF'); ?></span>
                                    <span class="tab-bar__nav-icon-cost"><?php echo current($mData['wpcf-packages-cost-per-day']); ?></span>
                                    <span class="tab-bar__nav-icon-per-day"><?php pll_e('per day'); ?></span>
                                </span>
                                <!--                                <span class="tab-bar__nav-title">&nbsp</span>-->
                                <span class="tab-bar__nav-text tab-bar__nav-text_gray"><?php echo $title; ?></span>
                            </div>
                        </li>
                        <?php $isFirst = false; } ?>
                </ul>
            </div>
            <div class="tab-bar__content">
                <?php
                $isFirst = true;
                foreach ($packages as $package) {
                    $mData = $package['metadata'];
                    $text = $package['DATA']->post_content;
                    $features = explode("\n", current($package['metadata']['wpcf-packages-feature']));
                    $title = '';
                    if (isset($package['DATA']->post_title)) {
                        $title = $package['DATA']->post_title;
                    }
                    ?>
                    <div class="tab-bar__item <?php if ($isFirst) { ?> tab-bar__item_active <?php  } ?> js-tab-content">
                        <div class="partner">
                            <h2 class="partner__title"><?php echo $title;?></h2>


                            <div class="partner__inner">
                                <div class="partner__container-slide">
                                    <div class="packages__container-slider-item"  >

                                        <p class="partner__slider-item-price"
                                        ><?php pll_e('From'); ?> <span class="partner__slider-item-dollar">CHF</span>
                                            <span class="partner__slider-item-count"><?php echo current($mData['wpcf-packages-cost-from']);
                                                ?></span> <?php pll_e('per day'); ?></p>
                                        <ul class="partner__slider-item-list">
                                            <li class="partner__slider-item-list-item"
                                            ><?php pll_e('Up to '); echo current($mData['wpcf-packages-number-of-users']); ?> <?php pll_e('users'); ?></li>
                                            <?php if (is_array($features) && !empty($features)) { ?>
                                                <?php foreach ($features as $feature) { ?>
                                                    <li class="partner__slider-item-list-item" ><?php echo $feature; ?></li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>

                                        <?php $imageArray = preparePackagesImages($mData);
                                        if (count($imageArray)){ ?>
                                            <h5 class="partner__slider-item-integration"><?php pll_e('integration'); ?></h5>
                                            <div class="partner__slider-item-logos">
                                                <?php foreach ($imageArray as $img) { ?>
                                                    <img src="<?php echo $img; ?>" class="partner__slider-item-logos_item">
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>

                                </div>
                                <div class="partner__container-list">
                                    <?php echo do_shortcode($text);?>
                                    <?php if (false) { ?>
                                        <!--                                        <h2 class="partner__list-item-title"></h2>-->
                                        <ul class="partner__list"   >
                                            <li class="partner__list-item">
                                                <!--
                                        <span
                                          class="partner__list-icon partner__list-icon_why"
                                          style="background-image: url('<?php //echo current($mData['']); ?>')"
                                        ></span>
                                        <div class="partner__list-info">
                                          <span class="partner__list-title"
                                              ><?php //echo current($mData['']); ?></span>
                                            <p class="partner__list-text">
                                                <?php //echo current($mData['']); ?>
                                            </p>
                                        </div>
                                        -->
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $isFirst = false; } ?>
            </div>
        </div>
    </div>
<?php } 