<?php
$postType = 'for-whom';
$keyOrder = 'wpcf-for-whom-order';
$postStatuses = array('publish', 'private');
$fields = array(
    'wpcf-for-whom-text',
    'wpcf-for-whom-image',
    'wpcf-for-whom-image-on-hover',
    'wpcf-for-whom-css-style-postfix',
);

$forWhoms = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null);

$_childs = array();
if(is_array($forWhoms)) {
    foreach($forWhoms as $forWhom) {
        $id = $forWhom['id'];
        $_ = getChildPostsIdsByParentId( $id, '_wpcf_belongs_for-whom_id' );

        $postType = 'for-whom-description';
        $keyOrder = 'wpcf-for-whom-description-order';
        $postStatuses = array('publish', 'private');
        $fields = array(
            'wpcf-for-whom-description-title',
            'wpcf-for-whom-description-text',
            'wpcf-for-whom-description-image',
        );
        $children = array();
        if (is_array($_) && !empty($_)) {
            $children = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null, false, $_);
        }
        $_childs[$id] = $children;
    }
}

if (is_array($forWhoms) && !empty($forWhoms)) { ?>
<div class="for-whom">
    <div class="for-whom__heading">
        <h1 class="for-whom__title"><?php strtoupper(pll_e('for whom')); ?></h1>
    </div>
    <div class="for-whom__tab-bar tab-bar">
        <div class="tab-bar__heading">
            <ul class="tab-bar__nav">

                <?php $isFirst = true;
                foreach($forWhoms as $forWhom ) {  $mData = $forWhom['metadata'];
                    $image = current($mData['wpcf-for-whom-image']);
                    $imageOnHover = current($mData['wpcf-for-whom-image-on-hover']);
                    $text = current($mData['wpcf-for-whom-text']);  ?>
                <li class="tab-bar__nav-item <?php if ($isFirst) { ?> tab-bar__nav-item_active <?php } ?> js-tab">
                    <div class="tab-bar__nav-inner">
                        <span class="tab-bar__nav-icon">
                            <span class="tab-bar__nav-icon-default"
                                <?php if ($image) { ?> style="background-image: url('<?php echo $image; ?>')" <?php } ?>
                            ></span>
                            <span class="tab-bar__nav-icon-hover"
                                  <?php if ($imageOnHover) { ?>
                                    style="background-image: url('<?php echo $imageOnHover; ?>');"
                                  <?php } ?>
                            ></span>
                        </span>
                        <span class="tab-bar__nav-title"><?php echo $forWhom['title']; ?></span>
                        <span class="tab-bar__nav-text"><?php echo $text; ?></span>
                    </div>
                </li>
                <?php $isFirst = false; } ?>
            </ul>
        </div>
        <div class="tab-bar__content">
            <?php if( is_array($_childs) && !empty($_childs)) {
                $isFirst = true;
                foreach ($_childs as $parentId => $masschild) {
                    $title = '';
                    $post = get_post($parentId);
                    if (isset($post->post_title)) {
                        $title = $post->post_title;
                    }
                    ?>

                <div class="tab-bar__item <?php if ($isFirst) { ?> tab-bar__item_active <?php  } ?> js-tab-content">
                    <div class="partner">
                        <h2 class="partner__title"><?php echo $title;?></h2>
                        <ul class="partner__list">
                          <?php foreach ($masschild as $_chld) { $mData = $_chld['metadata']; ?>
                              <li class="partner__list-item">
                                  <span
                                      class="partner__list-icon partner__list-icon_why"
                                      style="background-image: url('<?php echo current($mData['wpcf-for-whom-description-image']); ?>')"
                                  ></span>
                                  <div class="partner__list-info">
                                      <span class="partner__list-title"
                                      ><?php echo current($mData['wpcf-for-whom-description-title']); ?></span>
                                      <p class="partner__list-text">
                                          <?php echo current($mData['wpcf-for-whom-description-text']); ?>
                                      </p>
                                  </div>
                              </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
          <?php $isFirst = false; } ?>
      <?php } ?>
        </div>
    </div>
</div>
<?php }