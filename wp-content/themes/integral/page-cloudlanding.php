<?php

    global $currentPost;
    global $currentUrl;
    $showFooterContactForm = true;

    $shown = false;
    if ( 'enabled' == metaAttr($currentPost, 'wpcf-pages-only-show-about-us-block-on-this-page') ) {
        get_template_part('pages/about-us-cloudlanding');
    }

    if ( 'enabled' == metaAttr($currentPost, 'wpcf-pages-only-show-features-block-on-this-page') ) {
        get_template_part('pages/features-cloudlanding');
    }

    if ( 'enabled' == metaAttr($currentPost, 'wpcf-pages-only-show-contact-form-on-this-page') ) {
        get_template_part('pages/contact-cloudlanding');
        $showFooterContactForm = false;
    }

    if ( 'enabled' == metaAttr($currentPost, 'wpcf-pages-only-show-for-whom-block-on-this-page') ) {
        get_template_part('pages/for-whom-cloudlanding');
    }

    if ( 'enabled' == metaAttr($currentPost, 'wpcf-pages-only-show-packages-block-on-this-page') ) {
        get_template_part('pages/packages-cloudlanding');
    }

?>
<?php if(!$shown) { ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <!--
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <h2 class="entry-title"><?php the_title(); ?></h2>
            <div class="entry">
            </div>
        </div>
        -->
    <?php endwhile;?>
    <?php endif; ?>
<?php } ?>

<?php get_template_part('blocks/scroll_to_top_arrow'); ?>

<?php if ($showFooterContactForm) { ?>
<?php get_template_part('blocks/contacts-cloudlanding'); ?>
<?php } ?>

