$(function () {

    var singleSliderSettings = {
        dots: true,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    arrows: false
                }
            }
        ]
    };

    //banner slider
    var bannerSlider =  $('.js-banner-slider');
    if( bannerSlider.length ){
        bannerSlider.slick(singleSliderSettings);
    }

    //top slider
    var topSlider =  $('.js-top-slider');
    if( topSlider.length ){
        topSlider.slick(singleSliderSettings);
    }

    //reviews slider
    var reviewsSlider =  $('.js-reviews-slider');
    if( reviewsSlider.length ){
        reviewsSlider.slick(singleSliderSettings);
    }

    //packages slider
    var packagesSlider =  $('.js-packages-slider'),
        packagesSlidesNumber = parseInt($('#js-packages-slides-number').val());

    function slidesToShow() {
        if( packagesSlidesNumber > 5 ) {
            return 5;
        } else {
            return packagesSlidesNumber;
        }
    }

    function centerModeDesktop() {
        if ( packagesSlidesNumber === 4 ) {
            packagesSlider.addClass('packages__slider_not-centered');
            return false;
        } else {
            return true;
        }
    }

    function initialSlide() {
        if( packagesSlidesNumber >= 5 ) {
            return 2;
        } else if ( packagesSlidesNumber === 4 ){
            return 0;
        } else {
            return 1;
        }
    }


    if( packagesSlider.length ){
        packagesSlider.slick({
            centerMode: centerModeDesktop(),
            centerPadding: '0',
            initialSlide: initialSlide(),
            slidesToShow: slidesToShow(),
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        initialSlide: initialSlide(),
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        initialSlide: initialSlide(),
                        centerPadding: '60px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    //advantages touch event
    $('.features__block-item-hidden').one('touchstart', function(e) {
        e.preventDefault();
        $(this).addClass('features__block-item-hidden_show');
    });

    //features touch event
    $('.advantages__item-hidden').on('touchstart', function() {
        $(this).toggleClass('advantages__item-hidden_show');
    });

    //side menu
    var sidemenu = {
        isMenuOpened: false,
        init : function () {

            var menuBtn = $('.js-btn-menu');

            // make it work on touch screen device and mouse click
            menuBtn.on('touchstart click', function (e) {
                e.stopPropagation();
                e.preventDefault();

                if (!$('.js-side-menu').hasClass('header__side-menu_open')) {
                    sidemenu.showFullMenu();
                } else {
                    sidemenu.toggleMenu();
                }

            });

            sidemenu.bodyClick();

        },

        bodyClick: function () {

            $('html').bind('click',function (e) {
                if (sidemenu.isMenuOpened &&
                    !$(e.target).hasClass('js-side-menu-lang-toggle') ) {
                    sidemenu.hideMenu();
                }
            });

        },

        toggleMenu: function () {
            if (!sidemenu.isMenuOpened) {
                sidemenu.showFullMenu();
            } else {
                sidemenu.hideMenu();
            }
        },

        showFullMenu: function () {
            $('.js-side-menu').addClass('header__side-menu_open');
            $('html').addClass('side-menu-open');
            sidemenu.isMenuOpened = true;
        },

        hideMenu: function () {
            $('.js-side-menu').removeClass('header__side-menu_open');
            $('html').removeClass('side-menu-open');
            sidemenu.isMenuOpened = false;
        }
    };

    sidemenu.init();

    //custom scroll
    var customScroll = $('.js-custom-scroll');
    customScroll.customScrollbar({
        fixedThumbHeight: 33,
        setWidth: false,
        axis: 'y',
        scrollbarPossition: 'outside'
    });

    $(window).on('resize orientationchange', function() {
        customScroll.customScrollbar('resize', true);
    });

    //open lang
    $('.js-header-lang-toggle').click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('header__lang_active');
    });

    $(document.body).on('mousedown touchstart', function ( e ) {
        var target = $(e.target);
        if (!target.closest('.header__lang').is('.header__lang_active')) {
            $('.header__lang_active').removeClass('header__lang_active');
        }
    });

    $('.js-side-menu-lang-toggle').click(function (e) {
        e.preventDefault();
        $('.header__side-menu-lang-list').toggleClass('header__side-menu-lang-list_active');
    });

    //news block slider
    var newsSlider =  $('.js-news-slider');
    if( newsSlider.length ){
        newsSlider.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    }

    //partners slider
    var partnersSlider =  $('.js-partners-slider');
    if( partnersSlider.length ) {
        partnersSlider.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    dots: true,
                    arrows: false
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                    arrows: false
                }
            }
            ]
        });
    }

    //scroll arrow
    $('.scroll-arrow').click(function() {
        $('body,html').animate({
            scrollTop : 0
        }, 500);
    });


    // Show or hide Suffix Fields ( Contact Us)
    hideElement('.js-hidden-middle-name, .js-hidden-suffix, .js-hidden-interest');

    $('.js-add-suffix-field').click(function (e) {
        e.preventDefault();

        var middleName = document.getElementsByClassName('js-hidden-middle-name')[0];
        var suffix     =  document.getElementsByClassName('js-hidden-suffix')[0];

        if (middleName.style.display === 'none' && suffix.style.display === 'none') {
            middleName.style.display = 'block';
            suffix.style.display = 'block';
        } else {
            middleName.style.display = 'none';
            suffix.style.display = 'none';
        }
    });

    //Show or hide input Field if Other selected ( Contact us )
    $('.js-other-interest').change(function (e) {
        e.preventDefault();
        e.stopPropagation();

        if( $(this).find(':selected').val() === 'other') {
            $('.js-hidden-interest').show();
        } else {
            hideElement('.js-hidden-interest');
        }
    });

    // Email/phone-nubmer switcher
    $('.js-radio-button').change(checkInput);

    function checkInput() {
        var currentRadioBlock = '.contact-us__form-group_' + $(this).val();

        if( $(this).hasClass('radio-button-hidden')) {
            $('.js-radio-button').addClass('radio-button-hidden');
            $(this).removeClass('radio-button-hidden');
            $('.contact-us__form-group-tab').addClass('contact-us__form-group_hidden');
            $(currentRadioBlock).removeClass('contact-us__form-group_hidden');
        } else {
            $(this).addClass('radio-button-hidden');
            $('.contact-us__form-group-tab').removeClass('contact-us__form-group_hidden');
            $(currentRadioBlock).addClass('contact-us__form-group_hidden');
        }
    }

    //tabs
    var tabNavs = $('.js-tab'),
        activeTabNavClass = 'tab-bar__nav-item_active',
        tabs = $('.js-tab-content'),
        activeTabClass = 'tab-bar__item_active';

    tabNavs.off('click').on('click', onTabClick);

    function onTabClick(e) {
        var thisTab = $(this),
            thisTabIndex = tabNavs.index(thisTab);
        tabNavs.removeClass(activeTabNavClass);
        thisTab.addClass(activeTabNavClass);

        tabs.removeClass(activeTabClass);
        $(tabs[thisTabIndex]).addClass(activeTabClass);
    }

    //Helper
    function hideElement(selector) {
        $(selector).hide();
    }
});