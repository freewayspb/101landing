<?php

global $currentPost;
global $currentUrl;


$homeUrl = '/';
if (function_exists('pll_home_url')) {
    $homeUrl = pll_home_url();
}
$currentPost = null;
$currentUrl = $homeUrl;
if (!is_front_page()) {
    $currentPost = get_post();
    $currentUrl = get_permalink($currentPost->ID);
}

$postType = 'page';
$keyOrder = 'wpcf-global-order-in-nav-menu';
$postStatuses = array('publish', 'private');
$fields = array();
$showInNavMenu = true;// If we need only posts for navigation menu

$pages = getSortedPosts($postType, $keyOrder, $postStatuses, $fields, null, null, $showInNavMenu ); ?>
<header class="header">
    <div class="header__inner">
        <a class="header__menu-toggle js-btn-menu"></a>
        <a href="/" class="header__logo"></a>
        <div class="header__navigation">
            <ul class="header__menu js-header-menu">
                <?php if (is_array($pages) && !empty($pages)) { ?>
                    <?php foreach ($pages as $page) { ?>
                        <li class="header__menu-item">
                            <a href="<?php echo $page['url']; ?>"
                               class="header__menu-item-link <?php if ($currentUrl == $page['url']) { ?> header__menu-item-link_active <?php } ?>"
                            ><?php echo $page['title']; ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>

            <?php
            $currentLang = 'en';
            if (function_exists('pll_current_language')) {
                $currentLang = pll_current_language();
            }

            $languagesList = array();
            if (function_exists('pll_languages_list')) {
                $languagesList = pll_languages_list();
            }

            ?>
            <div class="header__lang">
                <a href="#" class="header__lang-toggle js-header-lang-toggle">
                    <?php if(is_front_page() && is_array($languagesList) && !empty($languagesList)) { ?>
                        <?php foreach ($languagesList as $oneLang) { $oneUrl = site_url() . '/' .$oneLang .'/'; ?>
                            <?php if($currentLang == $oneLang) { ?>
                            <img src="/wp-content/themes/integral/images/header/<?php echo $oneLang; ?>.svg" alt="" class="header__lang-toggle-icon" />
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>

                  <?php if ($currentPost && isset($currentPost->ID)) {
                        $postTranslations = wp_get_post_terms( $currentPost->ID, 'post_translations' );
                        $current = null;
                        $postLanguages = null;
                        if (is_array($postTranslations) ) {
                            $current = current($postTranslations);
                            if (isset($current->description)) {
                                $postLanguages = unserialize($current->description);
                                if (is_array($postLanguages) && !empty($postLanguages)) {
                                    foreach ($postLanguages as $langKey => $postId) {
                                        $link = get_permalink($postId); ?>
                                        <?php if($currentLang == $langKey) { ?>
                                                <img src="/wp-content/themes/integral/images/header/<?php echo $langKey; ?>.svg" alt="" class="header__lang-toggle-icon" />
                                        <?php } ?>
                                <?php } ?>
                            <?php } ?>
                    <?php }
                    }
                    } ?>
                    <?php } ?>
                    <span class="header__lang-toggle-arrow"></span>
                </a>
            <ul class="header__lang-list">
                <?php if(is_front_page() && is_array($languagesList) && !empty($languagesList)) { ?>
                    <?php foreach ($languagesList as $oneLang) { $oneUrl = site_url() . '/' .$oneLang .'/'; ?>
                            <li class="header__lang_list-item">
                                <a href="<?php echo $oneUrl; ?>" class="header__lang-list-link">
                                    <img class="header__lang-list-link-image" src="/wp-content/themes/integral/images/header/<?php echo $oneLang; ?>.svg" alt="">
                                    <?php echo getLanguageNameByCode($oneLang); ?>
                                </a>
                            </li>
                    <?php } ?>
                <?php } else { ?>
                    <?php
                    if ($currentPost && isset($currentPost->ID)) {
                        $postTranslations = wp_get_post_terms( $currentPost->ID, 'post_translations' );
                        $current = null;
                        $postLanguages = null;
                        if (is_array($postTranslations) ) {
                            $current = current($postTranslations);
                            if (isset($current->description)) {
                                $postLanguages = unserialize($current->description);
                                if (is_array($postLanguages) && !empty($postLanguages)) {
                                    foreach ($postLanguages as $langKey => $postId) {
                                        $link = get_permalink($postId); ?>
                                        <li class="header__lang_list-item">
                                            <a href="<?php echo $link; ?>" class="header__lang-list-link">
                                                <img class="header__lang-list-link-image" src="/wp-content/themes/integral/images/header/<?php echo $langKey; ?>.svg" alt="">
                                                <?php echo getLanguageNameByCode($langKey); ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                               <?php } ?>
                        <?php }
                        }
                    } ?>
                <?php } ?>
            </ul>
        </div>
            <a href="http://101cloud-portal-dev.azurewebsites.net/" target="_blank" class="header__login-btn"><?php pll_e('Login'); ?></a>
        </div>


        <!-- Mobile -->
        <div class="header__side-menu js-side-menu">
            <ul class="header__side-menu-list">

                <?php if (is_array($pages) && !empty($pages)) { ?>
                    <?php foreach ($pages as $page) { ?>
                        <li class="header__side-menu-list-item">
                            <a href="<?php echo $page['url']; ?>"
                               class="header__side-menu-list-item-link js-side-menu-link <?php if ($currentUrl == $page['url']) { ?> header__side-menu-list-item-link_active <?php } ?>"
                            ><?php echo $page['title']; ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>

                <li class="header__side-menu-list-item">
                    <a href="http://101cloud-portal-dev.azurewebsites.net/" target="_blank" class="header__side-menu-list-item-link js-side-menu-link"><?php pll_e('Login'); ?></a>
                </li>

                <li class="header__side-menu-list-item">
                    <a href="#" class="header__side-menu-list-item-link header__side-menu-list-item-link_arrow js-side-menu-lang-toggle">
                        Languages
                    </a>

                    <ul class="header__side-menu-lang-list">


                        <?php if(is_front_page() && is_array($languagesList) && !empty($languagesList)) { ?>
                            <?php foreach ($languagesList as $oneLang) { $oneUrl = site_url() . '/' .$oneLang .'/'; ?>

                                <li class="header__side-menu-lang-list-item">
                                    <a
                                        href="<?php echo $oneUrl; ?>"
                                        class="header__side-menu-lang-list-link <?php if($currentLang == $oneLang) { ?> header__side-menu-lang-list-link-active <?php } ?>">

                                        <img
                                            class="header__side-menu-lang-list-link-image"
                                            src="/wp-content/themes/integral/images/header/<?php echo $oneLang; ?>.svg"
                                            alt="<?php pll_e($oneLang); ?>">
                                        <?php pll_e($oneLang); ?>
                                    </a>
                                </li>

                            <?php } ?>
                        <?php } else { ?>
                            <?php
                            if ($currentPost && isset($currentPost->ID)) {
                                $postTranslations = wp_get_post_terms( $currentPost->ID, 'post_translations' );
                                $current = null;
                                $postLanguages = null;
                                if (is_array($postTranslations) ) {
                                    $current = current($postTranslations);
                                    if (isset($current->description)) {
                                        $postLanguages = unserialize($current->description);
                                        if (is_array($postLanguages) && !empty($postLanguages)) {
                                            foreach ($postLanguages as $langKey => $postId) {
                                                $link = get_permalink($postId); ?>

                                                <li class="header__side-menu-lang-list-item">
                                                    <a href="<?php echo $link; ?>" class="header__side-menu-lang-list-link <?php if($currentLang == $langKey) { ?> header__side-menu-lang-list-link-active <?php } ?>">
                                                        <img class="header__side-menu-lang-list-link-image" src="/wp-content/themes/integral/images/header/<?php echo $langKey; ?>.svg" alt="<?php pll_e(getLanguageNameByCode($langKey)); ?>">
                                                        <?php pll_e(getLanguageNameByCode($langKey)); ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php }
                                }
                            } ?>
                        <?php } ?>
                    </ul>
                </li>

                <?php if (false) { ?>
                <li class="header__side-menu-list-item">
                    <?php if(is_front_page() && is_array($languagesList) && !empty($languagesList)) { ?>
                        <?php foreach ($languagesList as $oneLang) { $oneUrl = site_url() . '/' .$oneLang .'/'; ?>
                            <a href="<?php echo $oneUrl; ?>" class="header__side-menu-lang <?php if($currentLang == $oneLang) { ?> header__side-menu-lang_active <?php } ?>"><?php pll_e(strtoupper($oneLang)); ?></a>
                        <?php } ?>
                    <?php } else { ?>
                        <?php
                        if ($currentPost && isset($currentPost->ID)) {
                            $postTranslations = wp_get_post_terms( $currentPost->ID, 'post_translations' );
                            $current = null;
                            $postLanguages = null;
                            if (is_array($postTranslations) ) {
                                $current = current($postTranslations);
                                if (isset($current->description)) {
                                    $postLanguages = unserialize($current->description);
                                    if (is_array($postLanguages) && !empty($postLanguages)) {
                                        foreach ($postLanguages as $langKey => $postId) {
                                            $link = get_permalink($postId); ?>
                                            <a
                                                href="<?php echo $link; ?>"
                                                class="header__side-menu-lang <?php if($currentLang == $langKey) { ?> header__side-menu-lang_active <?php } ?>"><?php pll_e(strtoupper($langKey)); ?></a>
                                        <?php } ?>
                                    <?php } ?>
                                <?php }
                            }
                        } ?>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</header>
