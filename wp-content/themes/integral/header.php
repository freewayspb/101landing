<?php
/**
 * Header section for our theme
 *
 * @package WordPress
 * @subpackage Integral
 * @since Integral 1.0
 */
?>
<?php global $integral; ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <title><?php pll_e('101Cloud Best solution for your business'); ?></title>
    <meta name="description" content="">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/integral/images/favicon/apple-touch-icon-152x152.png">
    <link rel="icon" sizes="16x16" href="/wp-content/themes/integral/images/favicon/favicon-16x16.png">
    <link rel="icon" sizes="32x32" href="/wp-content/themes/integral/images/favicon/favicon-32x32.png">
    <link rel="icon" sizes="96x96" href="/wp-content/themes/integral/images/favicon/favicon-96x96.png">
    <link rel="android-touch-icon" type="image/png" href="/wp-content/themes/integral/images/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="smart-banner-icon" href="/wp-content/themes/integral/images/favicon/icon.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/wp-content/themes/integral/images/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="shortcut icon" href="/wp-content/themes/integral/images/favicon/favicon.ico" type="image/x-icon">
<!--
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
-->
<?php
//    wp_enqueue_style( 'style.less', get_stylesheet_directory_uri() . '/less/style.less' );
    wp_enqueue_style( 'libs.css', get_stylesheet_directory_uri() . '/css/libs.css' );
    wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/less/main.less' );

    wp_enqueue_script( 'libs.js', get_stylesheet_directory_uri() . '/js/libs.js' );
    wp_enqueue_script( 'jquery.custom-scrollbar.min.js', get_stylesheet_directory_uri() . '/js/jquery.custom-scrollbar.min.js' );
    wp_enqueue_script( 'main.js', get_stylesheet_directory_uri() . '/js/main.js' );
?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>    
    <?php require_once dirname(__FILE__) . '/header-cloudlanding.php'; ?>
